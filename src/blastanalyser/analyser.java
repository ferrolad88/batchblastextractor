package blastanalyser;

import java.io.*;
import java.awt.FileDialog;

import javax.swing.JFrame;

import org.apache.commons.io.FileUtils;

public class analyser {
  public analyser() {
  }
  private String str = "";
  private int openFlag = 0;
  private int saveFlag = 0;
  String filePath, fileOutputPath;
  String filename;

  public static void main(String args[]) {

    String thisLine;

    //Open the file for reading
    try {
      @SuppressWarnings("resource")
	BufferedReader br = new BufferedReader(new FileReader(
          "blastx_Input.txt"));
      FileWriter fw = new FileWriter("blastx_Output.xls");
      BufferedWriter out = new BufferedWriter(fw);
      //writeLine(out,"<pre>");
      writeLine(out, "QueryID\tQueryLen\tAccession Version #\tLength\tScore\tbit\tEvalue\tIdentities\tPositives\tGaps\tOrganism\tDescription");
      String str = "";
      String str2 = "";
      int i=1;

      while ( (thisLine = br.readLine()) != null) { // while loop begins here

        if (thisLine.matches("^Query=.*$")) {
          //Star = thisLine.toString().substring(7, 18);

          String ab = thisLine.substring(7).trim(); // all
          ab = ab + " ";
          ab = ab.substring(0, ab.indexOf(" ")); // non contig

          str = ab.toString();//thisLine.toString().substring(7, 30);
          //Star = thisLine.toString().substring(7);


          //String str1 = thisLine.substring(7);
          //Star = str1.substring(0, str1.indexOf("\\s"));
          }
        else if (thisLine.matches("^.*letters\\).*$")) {
          int pos2 = thisLine.toString().indexOf("lett");
          str = str + "\t" + thisLine.toString().substring(10, pos2 - 1) ;
          str2 = str;
        }
        else if (thisLine.matches("^.*Sequences producing significant alignments:.*$")) { // Accession Version #
          int k = 0;

          // best 100, 20, 5, 1

          while ((k <1 && (!(thisLine.matches("^Query=.*$"))))){
            if (thisLine.matches("^>.*$")) { // Accession Version #

              String tmp = thisLine.substring(thisLine.indexOf("|")+1);

              /*do {
                thisLine = br.readLine();
                tmp = tmp + thisLine.toString();
              }
              while (! (thisLine.matches("^.*Length =.*$")));*/


              String tmpstr = tmp.substring(0, tmp.indexOf("|"));
              String desc,org =" ";

              if (tmp.matches("^.*\\[.*$")){
                desc = tmp.substring(tmp.indexOf("|") + 1,tmp.indexOf("["));
                org = tmp.substring(tmp.indexOf("[")+1);
              }else{
                desc = tmp.substring(tmp.indexOf("|") + 1);
              }

              if (org.matches("^.*\\].*$")){
                org = org.substring(0, org.indexOf("]"));
              }



              //str = str + "\t<a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=Protein&cmd=search&term="+tmpstr+"'>"+tmpstr+"</a>";
              str = str2 + "\t"+tmpstr;
              //System.out.println(str);
              k++;
              do {
                thisLine = br.readLine();
              }
              while (! (thisLine.matches("^.*Length =.*$")));
              str = str + "\t" + thisLine.substring(thisLine.indexOf("Length =") +9);
                do {
                  thisLine = br.readLine();
                }
                while (! (thisLine.matches("^.*Score =.*$")));
                str = str + "\t" + thisLine.substring((thisLine.indexOf("Score =")+7),(thisLine.indexOf("bits")))
                    +"\t"+ thisLine.substring((thisLine.indexOf("bits")+6),(thisLine.indexOf("Expect")-3))
                +"\t"+ thisLine.substring((thisLine.indexOf("Expect =")+8));

            thisLine = br.readLine(); // next line to Identities...

            String positives = "";
            String gaps = "";
            if (thisLine.matches("^.*Gaps =.*$")){
              positives = thisLine.substring((thisLine.indexOf("Positives =")+11),(thisLine.indexOf("Gaps =")-2));
              gaps = thisLine.substring((thisLine.indexOf("Gaps =")+6));
            }else{
              positives = thisLine.substring((thisLine.indexOf("Positives =")+11));
            }

            str = str + "\t" + thisLine.substring((thisLine.indexOf("Identities =")+12),(thisLine.indexOf("Positives =")-2))
                +"\t"+positives+"\t"+gaps+"\t"+org+ "\t"+desc ;


              /////////////////////////
              System.out.println(i+"\t"+str);
              //System.out.println(str);



              //System.out.println(str);
              writeLine(out, str);
              str = "\t";//str.substring(0, 15);
              i++;

              }
              /////////////////////////
           thisLine = br.readLine();
          }

          if (thisLine.matches("^Query=.*$")) {
            //str = thisLine.toString().substring(7, 18);

            String ab = thisLine.substring(7).trim(); // all
            ab = ab + " ";
            ab = ab.substring(0, ab.indexOf(" ")); // non contig


            str = ab.toString();//thisLine.toString().substring(7, 30);
            //str = thisLine.toString().substring(7);

            //String str1 = thisLine.substring(7);
            //str = str1.substring(0,str1.indexOf("\\s"));
         }


        }
       else if (thisLine.matches("^.*No hits found.*$")) {
         str = str +"\t ******No hits\t found.******";
        /////////////////////////

        System.out.println(i+"\t"+str);
        //System.out.println(str);
        writeLine(out, str);
        i++;

        }



      } // end while readLine

      out.close();
    } // end try
    catch (IOException e) {
      System.err.println("Error: " + e);
    }
    //} // end for

  } // end main

  public void openFileInput() {
    //openning the file dialog
    JFrame frame = new JFrame();
    FileDialog loadDialog = new FileDialog(frame, "Open File", FileDialog.LOAD);
    loadDialog.setVisible(true);
    //check for existance the file and correct selection
    if (loadDialog.getFile() != null) {
      openFlag = 1;
      filename = loadDialog.getDirectory() + loadDialog.getFile();
      filePath = filename;
      fileOutputPath = loadDialog.getDirectory();
    }
  }
  public void setFileOutput() {
    //openning the file dialog
    JFrame frame = new JFrame();
    FileDialog loadDialog = new FileDialog(frame, "Set File", FileDialog.SAVE);
    loadDialog.setVisible(true);
    //check for existance the file and correct selection
    if (loadDialog.getFile() != null) {
      openFlag = 1;
      fileOutputPath = loadDialog.getDirectory() + loadDialog.getFile();
      //filePath = filename;
    }
  }

    // Access modifiers
    public void setStr(String str) {
        this.str = str;
      }
    public String getStr() {
        return str;
      }
    public String getfilePath() {
        return filePath;
      }
    public String getfileOutputPath() {
        return fileOutputPath;
      }
    public void setOpenFlag(int openFlag) {
      this.openFlag = openFlag;
    }
    public int getOpenFlag() {
      return openFlag;
    }
    public void setSaveFlag(int saveFlag) {
      this.saveFlag = saveFlag;
    }
    public int getSaveFlag() {
      return saveFlag;
    }


  public void analyse(String Inputfilename1,String Outputfilename1,int NumberOfResults) {

    //analyser analyser = new analyser();
    //BatchBlastExtractor_Frame BatchBlastExtractor_Frame1 = new BatchBlastExtractor_Frame();
    //BatchBlastExtractor_Frame1.statusBar.setText("Processing...");
    String thisLine;
    String ab ="";

    //Open the file for reading
    try {
 
    	File f = new File(Inputfilename1+".tmp");
        String content = FileUtils.readFileToString(new File(Inputfilename1));
        String linesep =  System.lineSeparator();
        FileUtils.writeStringToFile(f, content.replaceAll(linesep+linesep+"Length=", linesep+"myletters="));
        
       
        
      BufferedReader br = new BufferedReader(new FileReader(Inputfilename1+".tmp"));
      FileWriter fw = new FileWriter(Outputfilename1);
      BufferedWriter out = new BufferedWriter(fw);
      //writeLine(out,"<pre>");
      writeLine(out, "QueryID\tQueryLen\tAccession Version #\tLength\tScore\tbit\tEvalue\tIdentities\tPositives\tGaps\tFrame\tOrganism\tDescription");
      String str = "";
      String str2 = "";
      int i=1;
      
        
      
  while ( (thisLine = br.readLine()) != null) { // while loop begins here
        if (thisLine.matches("^Query=.*$")) {
        	thisLine = thisLine.replaceAll(" ", "_");
          ab = thisLine.substring(7).trim(); // all
          ab = ab + " ";
          //ab = ab.substring(0, ab.indexOf(" ")); // non contig
          str = ab.toString();
          str2 = str;
          
        }
        else if (  thisLine.matches("^.*myletters=.*$") ) {
            //int pos2 = thisLine.toString().indexOf("Length");
          str = str + "\t" + thisLine.toString().substring(10).trim() ;
          str2 = str;
          
        }
              
       else if ( thisLine.matches("^.*letters\\).*$")) {
         int pos2 = thisLine.toString().indexOf("lett");
         str = str + "\t" + thisLine.toString().substring(10, pos2 - 1) ;
          str2 = str;
        }
        
        else if (thisLine.matches("^.*Sequences producing significant alignments:.*$")) { // Accession Version #
          int k = 0;
          // best 100, 20, 5, 1
          while (k < NumberOfResults && (!(thisLine.matches("^Query=.*$"))) && (!(thisLine.matches("Matrix: BLOSUM62"))) ){
            if (thisLine.matches("^>.*$")) { // Accession Version #
              String tmp = thisLine.substring(thisLine.indexOf("|")+1);
              String tmpstr = tmp.substring(0, tmp.indexOf("|"));
              String desc,org =" ";
              if (tmp.matches("^.*\\[.*$")){
                desc = tmp.substring(tmp.indexOf("|") + 1,tmp.indexOf("["));
                org = tmp.substring(tmp.indexOf("[")+1);
              }else{
                desc = tmp.substring(tmp.indexOf("|") + 1);
              }
              if (org.matches("^.*\\].*$")){
                org = org.substring(0, org.indexOf("]"));
              }
             str = str2 + "\t"+tmpstr;
              k++;

             do {
                thisLine = br.readLine();
              }
              while (! (thisLine.matches("^.*Length.*$")));

              thisLine = thisLine.replaceAll(" ", "");
                str = str + "\t" + thisLine.substring(thisLine.indexOf("Length") +7);
             
             do {
                  thisLine = br.readLine();
                }
                while (! (thisLine.matches("^.*Score =.*$")));
                thisLine = thisLine.replaceAll("  ", " ");
                str = str + "\t" + thisLine.substring((thisLine.indexOf("Score =")+7),(thisLine.indexOf("bits")))
                    +"\t"+ thisLine.substring((thisLine.indexOf("bits")+6),(thisLine.indexOf("Expect")-3))
                +"\t"+ thisLine.substring((thisLine.indexOf("Expect =")+8));

            thisLine = br.readLine(); // next line to Identities...
            String positives = "";
            String gaps = "";

            if (thisLine.matches("^.*Gaps =.*$")){
              positives = thisLine.substring((thisLine.indexOf("Positives =")+11),(thisLine.indexOf("Gaps =")-2));
              gaps = thisLine.substring((thisLine.indexOf("Gaps =")+6));
            }else{
              positives = thisLine.substring((thisLine.indexOf("Positives =")+11));
            }
            String frame = "";
            frame = br.readLine().toString();
            frame = frame.substring((frame.indexOf("Frame =")+7));

str = str + "\t" + thisLine.substring((thisLine.indexOf("Identities =")+12),(thisLine.indexOf("Positives =")-2))
                +"\t"+positives+"\t"+gaps+"\t"+frame+"\t"+org+ "\t"+desc ;
              writeLine1(out, str);
              System.out.println(i+"\t"+ab);
              str = "\t";
              i++;
              }
           thisLine = br.readLine();
          }
       
       if (thisLine.matches("^Query=.*$")) {
        	  thisLine = thisLine.replaceAll(" ", "_");
            ab = thisLine.substring(7).trim(); // all
            ab = ab + " ";
            //ab = ab.substring(0, ab.indexOf(" ")); // non contig
            str = ab.toString();
            str2 = str;
            
         }
        } // End of Accession Version 
        
       else if (thisLine.matches("^.*No hits found.*$")) {
         str = str +"\t ******No hits\t found.******";
         //System.out.println(i+"\t"+str);
         System.out.println(i+"\t"+ab);
        writeLine1(out, str);
        i++;
        }
      
      } // end while readLine
      
      
      System.out.println ("Done");
      out.close();
      
      br.close();
      f.delete();
       
      
    } // end try
    catch (IOException e) {
      System.err.println("Error: " + e);
    }
  }

  void writeLine1(BufferedWriter out, String line) throws IOException {
    out.write(line, 0, line.length());
    out.newLine();
  }

  static void writeLine(BufferedWriter out, String line) throws IOException {
    out.write(line, 0, line.length());
    out.newLine();
  }

}

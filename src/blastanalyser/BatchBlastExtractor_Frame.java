package blastanalyser;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JTextField;
import javax.swing.BorderFactory;

import java.awt.Color;

import javax.swing.border.TitledBorder;
import javax.swing.JComboBox;
import javax.swing.UIManager;

import java.awt.Font;

public class BatchBlastExtractor_Frame
    extends JFrame {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
JPanel contentPane;
  JMenuBar jMenuBar1 = new JMenuBar();
  JMenu jMenuFile = new JMenu();
  JMenuItem jMenuFileExit = new JMenuItem();
  JMenu jMenuHelp = new JMenu();
  JMenuItem jMenuHelpAbout = new JMenuItem();
  ImageIcon image1 = new ImageIcon(blastanalyser.BatchBlastExtractor_Frame.class.
                                   getResource("openFile.png"));
  ImageIcon image2 = new ImageIcon(blastanalyser.BatchBlastExtractor_Frame.class.
                                   getResource("closeFile.png"));
  ImageIcon image3 = new ImageIcon(blastanalyser.BatchBlastExtractor_Frame.class.
                                   getResource("help.png"));
  JLabel statusBar = new JLabel();
  JLabel jLabel1 = new JLabel();
  JTextField jTxtFieldInput = new JTextField();
  JButton jBtnInput = new JButton();
  JTextField jTxtFieldOutput = new JTextField();
  JLabel jLabel2 = new JLabel();
  JButton jBtnOutput = new JButton();
  JButton jBtnRun = new JButton();
  JButton jBtnExit = new JButton();
  TitledBorder titledBorder1 = new TitledBorder("");
  JLabel jLabel3 = new JLabel();
  String[] combos = {
         "Best Matches",
         "5",
         "20",
         "100"
     };
  @SuppressWarnings({ "unchecked", "rawtypes" })
JComboBox jComboBox1 = new JComboBox(combos);
  JLabel jLabel4 = new JLabel();
  JLabel jLabel5 = new JLabel();
  JLabel jLabel6 = new JLabel();
  analyser analyser1 = new analyser();
  public BatchBlastExtractor_Frame() {
    try {
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      jbInit();
    }
    catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  /**
   * Component initialization.
   *
   * @throws java.lang.Exception
   */
  private void jbInit() throws Exception {
    contentPane = (JPanel) getContentPane();
    contentPane.setLayout(null);
    setSize(new Dimension(580, 470));
    setTitle("Batch Blast Extractor");
    statusBar.setBackground(Color.gray);
    statusBar.setBorder(BorderFactory.createRaisedBevelBorder());
    statusBar.setToolTipText("");
    statusBar.setText(" Status Bar");
    statusBar.setBounds(new Rectangle( -4, 390, 591, 31));
    jMenuFile.setText("File");
    jMenuFileExit.setText("Exit");
    jMenuFileExit.addActionListener(new
                                    BatchBlastExtractor_Frame_jMenuFileExit_ActionAdapter(this));
    jMenuHelp.setText("Help");
    jMenuHelpAbout.setText("About");
    jMenuHelpAbout.addActionListener(new
                                     BatchBlastExtractor_Frame_jMenuHelpAbout_ActionAdapter(this));
    jLabel1.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
    jLabel1.setText("Select Output Directory:");
    jLabel1.setBounds(new Rectangle(50, 202, 323, 31));
    jTxtFieldInput.setBounds(new Rectangle(49, 68, 336, 24));
    jBtnInput.setBounds(new Rectangle(388, 68, 116, 24));
    jBtnInput.setText("Browse...");
    jBtnInput.addActionListener(new
        BatchBlastExtractor_Frame_jBtnInput_actionAdapter(this));
    jTxtFieldOutput.setBounds(new Rectangle(50, 236, 336, 24));
    jLabel2.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
    jLabel2.setText("Select Input file");
    jLabel2.setBounds(new Rectangle(49, 38, 323, 31));
    jBtnOutput.setBounds(new Rectangle(389, 236, 116, 24));
    jBtnOutput.setText("Browse...");
    jBtnOutput.addActionListener(new
        BatchBlastExtractor_Frame_jBtnOutput_actionAdapter(this));
    jBtnRun.setBounds(new Rectangle(92, 307, 209, 35));
    jBtnRun.setText("Run");
    jBtnRun.addActionListener(new
                              BatchBlastExtractor_Frame_jBtnRun_actionAdapter(this));
    jBtnExit.setBounds(new Rectangle(319, 307, 103, 35));
    jBtnExit.setText("Exit");
    jBtnExit.addActionListener(new
                               BatchBlastExtractor_Frame_jBtnExit_actionAdapter(this));
    jLabel3.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
    jLabel3.setText("Set the number of matches:");
    jLabel3.setBounds(new Rectangle(48, 135, 189, 31));
    jComboBox1.setBounds(new Rectangle(214, 141, 151, 24));
    jComboBox1.addActionListener(new
        BatchBlastExtractor_Frame_jComboBox1_actionAdapter(this));
    jLabel4.setBackground(UIManager.getColor("InternalFrame.borderLight"));
    jLabel4.setForeground(UIManager.getColor("inactiveCaptionText"));
    jLabel4.setBorder(BorderFactory.createEtchedBorder());
    jLabel4.setOpaque(true);
    jLabel4.setBounds(new Rectangle(40, 42, 473, 60));
    jLabel5.setBackground(UIManager.getColor("InternalFrame.borderLight"));
    jLabel5.setForeground(UIManager.getColor("inactiveCaptionText"));
    jLabel5.setBorder(BorderFactory.createEtchedBorder());
    jLabel5.setOpaque(true);
    jLabel5.setBounds(new Rectangle(40, 208, 473, 60));
    jLabel6.setBackground(UIManager.getColor("InternalFrame.borderLight"));
    jLabel6.setBorder(BorderFactory.createEtchedBorder());
    jLabel6.setOpaque(true);
    jLabel6.setBounds(new Rectangle(40, 133, 341, 42));
    jMenuBar1.add(jMenuFile);
    jMenuFile.add(jMenuFileExit);
    jMenuBar1.add(jMenuHelp);
    jMenuHelp.add(jMenuHelpAbout);
    setJMenuBar(jMenuBar1);
    contentPane.add(statusBar, null);
    contentPane.add(jBtnRun);
    contentPane.add(jTxtFieldInput);
    contentPane.add(jLabel2);
    contentPane.add(jBtnInput);
    contentPane.add(jLabel3);
    contentPane.add(jComboBox1);
    contentPane.add(jTxtFieldOutput);
    contentPane.add(jLabel1);
    contentPane.add(jBtnOutput);
    contentPane.add(jLabel4);
    contentPane.add(jLabel5);
    contentPane.add(jLabel6);
    contentPane.add(jBtnExit);
  }

  /**
   * File | Exit action performed.
   *
   * @param actionEvent ActionEvent
   */
  void jMenuFileExit_actionPerformed(ActionEvent actionEvent) {
    System.exit(0);
  }

  /**
   * Help | About action performed.
   *
   * @param actionEvent ActionEvent
   */
  void jMenuHelpAbout_actionPerformed(ActionEvent actionEvent) {
    BatchBlastExtractor_Frame_AboutBox dlg = new
        BatchBlastExtractor_Frame_AboutBox(this);
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Point loc = getLocation();
    dlg.setLocation( (frmSize.width - dlgSize.width) / 2 + loc.x,
                    (frmSize.height - dlgSize.height) / 2 + loc.y);
    dlg.setModal(true);
    dlg.pack();
    dlg.setVisible(true);
  }

  public void jComboBox1_actionPerformed(ActionEvent e) {

  }

  public void jBtnExit_actionPerformed(ActionEvent e) {
/////////////////////////////////
    System.exit(0);
  }

  public void jBtnInput_actionPerformed(ActionEvent e) {
/////////////////////////////
    analyser1.openFileInput();
    if (analyser1.getOpenFlag() == 1) {
      statusBar.setText("  File Selected");
      jTxtFieldInput.setText(analyser1.getfilePath());
      jTxtFieldOutput.setText(analyser1.getfileOutputPath() + "BlastOutput.xls");
    }
    else statusBar.setText("  Input file has not been selected yet.");
  }

  public void jBtnOutput_actionPerformed(ActionEvent e) {
///////////////////////////////
    analyser1.setFileOutput();
    jTxtFieldOutput.setText(analyser1.getfileOutputPath());
  }

  public void jBtnRun_actionPerformed(ActionEvent e) {
///////////////////////////////////
    int comboIndexInt = jComboBox1.getSelectedIndex();
    int comboIndex = 0;
    switch(comboIndexInt) {
             case 0:
                comboIndex = 1;
                break;
             case 1:
                comboIndex = 5;
                break;
            case 2:
                comboIndex = 20;
                break;
              case 3:
                 comboIndex = 25;
                 break;
         }

    analyser1.analyse(jTxtFieldInput.getText(),jTxtFieldOutput.getText(),comboIndex);
  }
}

class BatchBlastExtractor_Frame_jBtnRun_actionAdapter
    implements ActionListener {
  private BatchBlastExtractor_Frame adaptee;
  BatchBlastExtractor_Frame_jBtnRun_actionAdapter(BatchBlastExtractor_Frame
                                                  adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.jBtnRun_actionPerformed(e);
  }
}

class BatchBlastExtractor_Frame_jBtnOutput_actionAdapter
    implements ActionListener {
  private BatchBlastExtractor_Frame adaptee;
  BatchBlastExtractor_Frame_jBtnOutput_actionAdapter(BatchBlastExtractor_Frame
      adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.jBtnOutput_actionPerformed(e);
  }
}

class BatchBlastExtractor_Frame_jBtnInput_actionAdapter
    implements ActionListener {
  private BatchBlastExtractor_Frame adaptee;
  BatchBlastExtractor_Frame_jBtnInput_actionAdapter(BatchBlastExtractor_Frame
      adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.jBtnInput_actionPerformed(e);
  }
}

class BatchBlastExtractor_Frame_jBtnExit_actionAdapter
    implements ActionListener {
  private BatchBlastExtractor_Frame adaptee;
  BatchBlastExtractor_Frame_jBtnExit_actionAdapter(BatchBlastExtractor_Frame
      adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.jBtnExit_actionPerformed(e);
  }
}

class BatchBlastExtractor_Frame_jComboBox1_actionAdapter
    implements ActionListener {
  private BatchBlastExtractor_Frame adaptee;
  BatchBlastExtractor_Frame_jComboBox1_actionAdapter(BatchBlastExtractor_Frame
      adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent e) {
    adaptee.jComboBox1_actionPerformed(e);
  }
}

class BatchBlastExtractor_Frame_jMenuFileExit_ActionAdapter
    implements ActionListener {
  BatchBlastExtractor_Frame adaptee;

  BatchBlastExtractor_Frame_jMenuFileExit_ActionAdapter(
      BatchBlastExtractor_Frame adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent actionEvent) {
    adaptee.jMenuFileExit_actionPerformed(actionEvent);
  }
}

class BatchBlastExtractor_Frame_jMenuHelpAbout_ActionAdapter
    implements ActionListener {
  BatchBlastExtractor_Frame adaptee;

  BatchBlastExtractor_Frame_jMenuHelpAbout_ActionAdapter(
      BatchBlastExtractor_Frame adaptee) {
    this.adaptee = adaptee;
  }

  public void actionPerformed(ActionEvent actionEvent) {
    adaptee.jMenuHelpAbout_actionPerformed(actionEvent);
  }
}
